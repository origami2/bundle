#! /bin/bash

. base.sh

for d in $dirs; do
  echo $d:
  (cd $d && eslint .)
done
