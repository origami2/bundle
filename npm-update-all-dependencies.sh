#! /bin/bash

. base.sh

npm install -g npm-check-updates

for d in $dirs; do
  echo $d:
  (cd $d && ncu -u)
done
