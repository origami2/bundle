#! /bin/bash

. base.sh

all=""

for d in $dirs; do
  if [ "$d" != "stack-io" ] && [ "$d" != "plugin-io" ] && [ "$d" != "token-provider-io" ]
  then
    all="$all $d/test"
  fi
done

# mocha -R dot -w -G $all
mocha -w -G $all
