#! /bin/bash

. base.sh

for d in $dirs; do
  echo $d:
  (cd $d && rm -fr node_modules)
done
