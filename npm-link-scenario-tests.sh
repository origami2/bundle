#! /bin/bash

(cd stack && npm link)
(cd plugin && npm link)
(cd token-provider && npm link)
(cd token-provider-client && npm link)
(cd client && npm link)
	
(cd scenario-tests && npm link origami-stack)
(cd scenario-tests && npm link origami-plugin)
(cd scenario-tests && npm link origami-token-provider)
(cd scenario-tests && npm link origami-token-provider-client)
(cd scenario-tests && npm link origami-client)
