#! /bin/bash

. base.sh

for d in $dirs; do
  echo $d:
  (cd $d && npm install && npm install --only=dev)
done
