#! /bin/bash

. base.sh


for d in $dirs; do
  echo $d:
  (git clone git@gitlab.com:origami2/$d.git $d)
done
