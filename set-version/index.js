var async = require('async');

(function () {
  var argv = require('minimist')(process.argv.slice(2));
  
  var version = argv._[0];
  
  function exitWith(msg) {
    if (msg) process.stderr.write(msg + '\n');
    process.exit(-1);
  }
  
  if (!version) exitWith('no version found');
  
  if (!(require('semver').valid(version))) return exitWith('invalid version');
  
  var paths = argv._.slice(1);
  
  if (!paths.length) return exitWith('no paths specified');
  
  async
  .map(
    paths,
    function (inputPath, callback) {
      var fs = require('fs');
      var path = require('path');
      
      fs.lstat(inputPath, function (err, stats) {
        if (err) return callback(err);
        
        if (stats.isDirectory()) {
          inputPath = path.join(inputPath, 'package.json');

          fs.lstat(inputPath, function (err, stats) {
            if (stats.isFile()) {
              callback(null, path.resolve(inputPath));
            }
          });
        } else if (stats.isFile()) {
          callback(null, path.resolve(inputPath));
        } else {
          callback('unkown file');
        }
      });
    },
    function (err, paths) {
      if (err) return exitWith(err);
      
      async
      .map(
        paths,
        function (path, callback) {
          var orig = require(path);
          
          if (orig.name.indexOf('origami-') !== 0) return callback('not an origami package');
          
          orig.version = version;
          
          var deps = orig.dependencies || {};
          
          for (var k in deps) {
            if (k.indexOf('origami-') !== 0) continue;
            
            deps[k] = version;
          }
          
          var devDeps = orig.devDependencies || {};
          
          for (var k in devDeps) {
            if (k.indexOf('origami-') !== 0) continue;

            devDeps[k] = version;
          }
          
          callback(null, [path, JSON.stringify(orig, null, 2)]);
        },
        function (err, map) {
          if (err) return exitWith(err);
          
          async
          .each(
            map,
            function (map, callback) {
              var fs = require('fs');
              
              fs.writeFile(map[0], map[1], callback);
            },
            function (err) {
              if (err) return exitWith(err);
              
              process.exit(0);
            }
          );
        }
      );
    }
  );
})();