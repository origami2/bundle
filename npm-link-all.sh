#! /bin/bash

. base.sh

for d in $dirs; do
  echo $d:
  (cd $d && npm link)
done

for d in $dirs; do
  echo $d:
  origamiDeps=`node ./list-origami-deps $d`
  
  for d2 in $origamiDeps; do
    (cd $d && npm link $d2)
  done
done