#! /bin/bash

. base.sh

for d in $dirs; do
  echo $d:
  (cd $d && npm publish)
done
