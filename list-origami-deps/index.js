var async = require('async');

(function () {
  var argv = require('minimist')(process.argv.slice(2));
  
  function exitWith(msg) {
    if (msg) process.stderr.write(msg + '\n');
    process.exit(-1);
  }
  
  var paths = argv._;
  
  if (!paths.length) return exitWith('no paths specified');
  
  async
  .map(
    paths,
    function (inputPath, callback) {
      var fs = require('fs');
      var path = require('path');
      
      fs.lstat(inputPath, function (err, stats) {
        if (err) return callback(err);
        
        if (stats.isDirectory()) {
          inputPath = path.join(inputPath, 'package.json');

          fs
          .lstat(inputPath, function (err, stats) {
            if (err) return callback(err);
            
            if (stats.isFile()) {
              callback(null, path.resolve(inputPath));
            }
          });
        } else if (stats.isFile()) {
          callback(null, path.resolve(inputPath));
        } else {
          callback('unkown file');
        }
      });
    },
    function (err, paths) {
      if (err) return exitWith(err);
      
      async
      .map(
        paths,
        function (path, callback) {
          var orig = require(path);
          var deps = [];
          
          for (var k in (orig.dependencies || {})) {
            if (k.indexOf('origami-') !== 0) continue;
            
            deps.push(k);
          }
          
          for (var k in (orig.devDependencies || {})) {
            if (k.indexOf('origami-') !== 0) continue;
            
            if (deps.indexOf(k) === -1) deps.push(k);
          }
          
          callback(null, deps);
        },
        function (err, deps) {
          if (err) return exitWith(err);
          
          for (var i = 0; i < deps.length; i++) {
            process.stdout.write(deps[i].join('\n') + '\n');
          }
        }
      );
    }
  );
})();